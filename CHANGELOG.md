# Changelog

## 0.0.3 - 2019-11-11

- **Notes**:

  - No behavior changed.
  - The project development & test environments are maintained using rit
    tunnel tool.

## 0.0.2 - 2019-09-11

- **Added**:

  - Add `FluxDatabase` behavior on `FluxDatabase` `__using__` macro.
  - Add code which creates [DETS](http://erlang.org/doc/man/dets.html) table
    directory before opening table in `FluxDatabase.DETS.start/1` pipeline.

## 0.0.1 - 2019-08-31

- **Added**:

  - The project is available on GitLab and Hex.
  - Every logger uses metadata to describe information. There are three
    metadata keys that can be filtered on logger settings:

    1. `:flux_database_params` - The params received by function.
    2. `:flux_database_result` - The result of the function.
    3. `:error` - The error result of the function.

- **Notes**:

  - Minor changes (0.0.x) from the current version will be logged to this file.

  - When a major change is released (0.x.0 or x.0.0), the changelog of the
    previous major change will be grouped as a single change.
