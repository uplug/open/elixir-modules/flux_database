# Flux Database

[![pipeline status](https://gitlab.com/uplug/open/elixir-modules/flux_database/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_database/commits/master)
[![coverage report](https://gitlab.com/uplug/open/elixir-modules/flux_database/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_database/commits/master)

Integrate databases to Elixir projects.

## Usage

Add [Flux Database](https://hex.pm/packages/flux_database) as a dependency in
your `mix.exs` file:

```elixir
def deps do
  [{:flux_database, "~> 0.0.3"}]
end
```

`FluxDatabase` describes how to create a module in a project to integrate
databases.

`FluxDatabase.DETS` describes how to use the
[DETS](http://erlang.org/doc/man/dets.html) integration.

`FluxDatabase.Mnesia` describes how to use the
[Mnesia](http://erlang.org/doc/man/mnesia.html) integration.

## Application Configuration

There are no application configurations. All the configuration is set by
extending `FluxDatabase` in a module.
