import Config

config :logger,
  backends: [:console],
  level: :debug,
  truncate: :infinity,
  discard_threshold: 5_000,
  handle_otp_reports: false

config :logger, :console,
  format: "$levelpad[$level] $message $metadata\n",
  metadata: [
    :flux_database_params,
    :flux_database_result,
    :error
  ]
