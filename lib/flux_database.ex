defmodule FluxDatabase do
  @moduledoc """
  Integrate databases to Elixir projects.

  The main approach to integrate a database is done by defining a module which
  extends `FluxDatabase` and implements `c:config/0` function:

  ```elixir
  defmodule MyApp.Database do
    use FluxDatabase

    @impl FluxDatabase
    def config do
      [
        database: :mnesia,
        nodes: [node()],
        tables: %{
          user: {
            :user,
            attributes: [:id, :name, :email]
          }
        }
      ]
    end
  end
  ```

  Then, call `MyApp.Database.start/0` somewhere which can start the database.
  It can be, for example, on your `Application` module:

  ```elixir
  defmodule MyApp.Application do
    use Application

    @impl Application
    def start(_type, _args) do
      MyApp.Database.start()

      children = [
      ]

      opts = [strategy: :one_for_one]

      Supervisor.start_link(children, opts)
    end
  end
  ```

  For more information about how to integrate and use
  [DETS](http://erlang.org/doc/man/dets.html) with `FluxDatabase`, check
  `FluxDatabase.DETS`.

  For more information about how to integrate and use
  [Mnesia](http://erlang.org/doc/man/mnesia.html) with `FluxDatabase`, check
  `FluxDatabase.Mnesia`.
  """

  @callback config() :: keyword()

  defmacro __using__(_opts) do
    quote do
      alias FluxDatabase.{DETS, Mnesia}

      @behaviour FluxDatabase

      def start do
        do_start(Keyword.get(config(), :database))
      end

      defp do_start(:dets) do
        DETS.start(Keyword.get(config(), :tables))
      end

      defp do_start(:mnesia) do
        spawn(fn ->
          Mnesia.start(Keyword.get(config(), :nodes), Keyword.get(config(), :tables))
        end)
      end

      defp do_start(_database) do
        {:error, :unknown_database}
      end

      def write(table_key, data) do
        do_write(Keyword.get(config(), :database), table_key, data)
      end

      defp do_write(:dets, table_key, data) do
        DETS.write(Keyword.get(config(), :tables), table_key, data)
      end

      defp do_write(:mnesia, table_key, data) do
        Mnesia.write(Keyword.get(config(), :tables), table_key, data)
      end

      defp do_write(_database, _table_key, _data) do
        {:error, :unknown_database}
      end

      def read(table_key, id) do
        do_read(Keyword.get(config(), :database), table_key, id)
      end

      defp do_read(:dets, table_key, id) do
        DETS.read(Keyword.get(config(), :tables), table_key, id)
      end

      defp do_read(:mnesia, table_key, id) do
        Mnesia.read(Keyword.get(config(), :tables), table_key, id)
      end

      defp do_read(_database, _table_key, _id) do
        {:error, :unknown_database}
      end

      def filter(table_key, filter_param) do
        do_filter(Keyword.get(config(), :database), table_key, filter_param)
      end

      defp do_filter(:dets, table_key, matchspecs) do
        DETS.filter(Keyword.get(config(), :tables), table_key, matchspecs)
      end

      defp do_filter(_database, _table_key, _filter_param) do
        {:error, :unknown_database}
      end

      def match(table_key, matcher) do
        do_match(Keyword.get(config(), :database), table_key, matcher)
      end

      defp do_match(:mnesia, table_key, matcher) do
        Mnesia.match(Keyword.get(config(), :tables), table_key, matcher)
      end

      defp do_match(_database, _table_key, _matcher) do
        {:error, :unknown_database}
      end
    end
  end
end
