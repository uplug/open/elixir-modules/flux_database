defmodule FluxDatabase.DETS do
  @moduledoc """
  [DETS](http://erlang.org/doc/man/dets.html) integration on `FluxDatabase`.

  ## Configuration

  The configuration expects a keyword list with:

  - `:database` - To be `:dets`.

  - `:tables` - A map with database definitions with
    `{path, specification, index_key}` as value and `table_key` as key.

    - `path` - An atom with the absolute path to the table storage.

    - `specification` - A keyword list with
      [:dets.open_file/2](http://erlang.org/doc/man/dets.html#open_file-2)
      arguments.

    - `index_key` - A string or atom defining the index key of the table.

  ### Example

  ```elixir
  defmodule MyApp.Database do
    use FluxDatabase

    @impl FluxDatabase
    def config do
      [
        database: :dets,
        tables: %{
          user: {
            :"/path/to/storage/user",
            [type: :set],
            :id
          },
          post: {
            :"/path/to/storage/post",
            [type: :set],
            :id
          }
        }
      ]
    end
  end
  ```

  ## Usage

  Whenever you need to use a function defined here, use it without the `tables`
  parameter and by your database module. For example, if you defined your
  database module as `MyApp.Database`, you can use:

  ```elixir
  alias MyApp.Database

  # To start your application
  Database.start()

  # To retrieve an item from table
  Database.read(:user, 1)

  # To filter items on table
  Database.filter(:user, Database.all_matchspecs())

  # To write an item on table
  Database.write(:user, %{id: 2, name: "John Doe", email: "john@doe.com"})
  ```

  Check the functions defined on this module for more information
  about how to use the functions in your database module.
  """

  @moduledoc since: "0.0.1"

  alias FluxDatabase.DETS.{Filter, Reader, Starter, Writer}

  require Logger

  @doc """
  Open all tables using
  [:dets.open_file/1](http://erlang.org/doc/man/dets.html#open_file-1).

  Use it from your database module with no parameters.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.DETS`.

  ## Examples

      iex> tables = %{user: {:"/path/to/storage/user", [type: :set], :id}}
      ...> FluxDatabase.DETS.start(tables)
      :ok
  """

  @doc since: "0.0.1"

  @spec start(map()) :: :ok | {:error, atom()}
  def start(tables) do
    Starter.start(tables)
  end

  @doc """
  Retrieve an item from table using
  [:dets.lookup/2](http://erlang.org/doc/man/dets.html#lookup-2).

  Use it from your database module without `tables` parameter.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.DETS`.

    - `table_key` - A string or atom with the name of the table.

    - `id` - The identifier value of the item.

  ## Examples

      iex> tables = %{user: {:"/path/to/storage/user", [type: :set], :id}}
      ...> FluxDatabase.DETS.start(tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.DETS.write(tables, :user, user)
      ...> FluxDatabase.DETS.read(tables, :user, 1)
      {:ok, %{id: 1, name: "John Doe", email: "john@doe.com"}}
  """

  @doc since: "0.0.1"

  @spec read(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def read(tables, table_key, id) do
    Reader.read(tables, table_key, id)
  end

  @doc """
  Filter items on table using
  [:dets.select/2](http://erlang.org/doc/man/dets.html#select-2).

  Use it from your database module without `tables` parameter.

  It uses
  [matchspecs](http://erlang.org/doc/apps/erts/match_spec.html#ets-examples).

  To get all items from your table, `all_matchspecs/0` can be used.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.DETS`.

    - `table_key` - A string or atom with the name of the table.

    - `matchspecs` - A
      [matchspecs](http://erlang.org/doc/apps/erts/match_spec.html#ets-examples)
      definition.

  ## Examples

      iex> tables = %{user: {:"/path/to/storage/user", [type: :set], :id}}
      ...> FluxDatabase.DETS.start(tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.DETS.write(tables, :user, user)
      ...> user = %{id: 2, name: "John Anderson", email: "neo@matrix.com"}
      ...> FluxDatabase.DETS.write(tables, :user, user)
      ...> FluxDatabase.DETS.filter(tables, :user, FluxDatabase.DETS.all_matchspecs())
      {
        :ok,
        [
          %{id: 1, name: "John Doe", email: "john@doe.com"},
          %{id: 2, name: "John Anderson", email: "neo@matrix.com"}
        ]
      }
  """

  @doc since: "0.0.1"

  @spec filter(map(), atom() | binary(), [any()]) :: {:ok, [map()]} | {:error, atom()}
  def filter(tables, table_key, matchspecs) do
    Filter.filter(tables, table_key, matchspecs)
  end

  @doc """
  Default
  [matchspec](http://erlang.org/doc/apps/erts/match_spec.html#ets-examples) to
  retrieve all items from a table using it in `filter/3`.

  Use it normally by calling from this module.

  ## Examples

      iex> FluxDatabase.DETS.all_matchspecs()
      [{{:_, :"$1"}, [], [:"$1"]}]
  """

  @doc since: "0.0.1"

  @spec all_matchspecs :: [{{:_, :"$1"}, [], [:"$1"]}]
  def all_matchspecs do
    Filter.all_matchspecs()
  end

  @doc """
  Write an item on table using
  [:dets.insert/2](http://erlang.org/doc/man/dets.html#insert-2).

  It merges the new data with the data written on table before writing the new
  data.

  Use it from your database module without `tables` parameter.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.DETS`.

    - `table_key` - A string or atom with the name of the table.

    - `data` - A map with the values that will be inserted on storage. Must
      have the `index_key` defined on tables settings. More information in
      `FluxDatabase.DETS`.

  ## Examples

      iex> tables = %{user: {:"/path/to/storage/user", [type: :set], :id}}
      ...> FluxDatabase.DETS.start(tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.DETS.write(tables, :user, user)
      {:ok, %{id: 1, name: "John Doe", email: "john@doe.com"}}
  """

  @doc since: "0.0.1"

  @spec write(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def write(tables, table_key, data) do
    Writer.write(tables, table_key, data)
  end
end
