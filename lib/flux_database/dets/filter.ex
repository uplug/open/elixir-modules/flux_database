defmodule FluxDatabase.DETS.Filter do
  @moduledoc false

  alias FluxDatabase.DETS.Utils

  require Logger

  @spec filter(binary(), [any()]) :: {:ok, [map()]} | {:error, atom()}
  def filter(table_path, matchspecs) do
    result = :dets.select(table_path, matchspecs)
    {:ok, result}
  rescue
    error ->
      Logger.error("DETS failed to filter",
        error: inspect(error),
        flux_database_params:
          inspect(
            table_path: table_path,
            matchspecs: matchspecs
          )
      )

      {:error, :dets_failed_to_filter}
  end

  @spec filter(map(), atom() | binary(), [any()]) :: {:ok, [map()]} | {:error, atom()}
  def filter(tables, table_key, matchspecs) do
    case Utils.get_table(tables, table_key) do
      {:ok, table_path, _index_key} ->
        filter(table_path, matchspecs)

      error ->
        Logger.error("DETS failed to get table data",
          error: inspect(error),
          flux_database_params: inspect(table_key: table_key)
        )

        {:error, :dets_failed_to_get_table_data}
    end
  end

  @spec all_matchspecs :: [{{:_, :"$1"}, [], [:"$1"]}]
  def all_matchspecs, do: [{{:_, :"$1"}, [], [:"$1"]}]
end
