defmodule FluxDatabase.DETS.Reader do
  @moduledoc false

  alias FluxDatabase.DETS.Utils

  require Logger

  @spec read(binary(), any()) :: {:ok, map()} | {:error, atom()}
  def read(table_path, id) do
    case :dets.lookup(table_path, id) do
      [{_key, element}] -> {:ok, element}
      [] -> {:error, :not_found}
    end
  end

  @spec read(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def read(tables, table_key, id) do
    case Utils.get_table(tables, table_key) do
      {:ok, table_path, _index_key} ->
        read(table_path, id)

      error ->
        Logger.error("DETS failed to get table data",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_key: table_key,
              id: id
            )
        )

        {:error, :dets_failed_to_get_table_data}
    end
  end
end
