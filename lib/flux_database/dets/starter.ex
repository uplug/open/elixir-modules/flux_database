defmodule FluxDatabase.DETS.Starter do
  @moduledoc false

  require Logger

  @spec start(map()) :: :ok | {:error, atom()}
  def start(tables) do
    case open_tables(tables) do
      :ok ->
        Logger.info("DETS database ready to use",
          flux_database_result: :ok,
          flux_database_params: inspect(tables)
        )

        :ok

      error ->
        Logger.error("DETS database failed to start",
          error: inspect(error),
          flux_database_params: inspect(tables)
        )

        error
    end
  end

  defp open_tables(tables) do
    Enum.each(tables, &open_table!/1)
  rescue
    _error -> {:error, :failed_to_open_tables}
  end

  defp open_table!({_key, {path, specifications, _index_key}}) do
    File.mkdir_p(Path.dirname(to_string(path)))
    {:ok, _path} = :dets.open_file(path, specifications)
  end
end
