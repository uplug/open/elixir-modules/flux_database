defmodule FluxDatabase.DETS.Utils do
  @moduledoc false

  @spec get_table(map(), atom() | binary()) ::
          {:ok, binary(), atom() | binary()} | {:error, any()}
  def get_table(tables, table_key) do
    case Map.get(tables, table_key) do
      {table_path, _specifications, index_key} -> {:ok, table_path, index_key}
      nil -> {:error, :dets_table_not_found}
      _ -> {:error, :dets_wrong_tables_structure}
    end
  end
end
