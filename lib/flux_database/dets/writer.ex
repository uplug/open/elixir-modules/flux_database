defmodule FluxDatabase.DETS.Writer do
  @moduledoc false

  alias FluxDatabase.DETS.{Reader, Utils}

  require Logger

  @spec write(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def write(tables, table_key, data) do
    with {:ok, table_path, index_key} <- Utils.get_table(tables, table_key),
         {:ok, index} <- get_data_index(data, index_key),
         {:ok, saved_data} <- get_saved_data(table_path, index),
         data <- Map.merge(saved_data, data),
         :ok <- :dets.insert(table_path, {index, data}) do
      {:ok, data}
    else
      error ->
        Logger.error("DETS failed to write data",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_key: table_key,
              data: data
            )
        )

        {:error, :dets_failed_to_write}
    end
  end

  defp get_data_index(data, index_key) do
    case Map.get(data, index_key) do
      nil -> {:error, :data_without_index_key}
      index -> {:ok, index}
    end
  end

  defp get_saved_data(table_path, index) do
    case Reader.read(table_path, index) do
      {:error, :not_found} -> {:ok, %{}}
      result -> result
    end
  end
end
