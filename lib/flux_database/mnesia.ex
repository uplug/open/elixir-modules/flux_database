defmodule FluxDatabase.Mnesia do
  @moduledoc """
  [Mnesia](http://erlang.org/doc/man/mnesia.html) integration on
  `FluxDatabase`.

  ## Configuration

  The configuration expects a keyword list with:

  - `:database` - To be `:mnesia`.

  - `:nodes` - A list of elixir nodes definition. Should have at least one.
    To define only the module node, use `[node()]`.

  - `:tables` - A map with database definitions with
    `{table_name, specifications}` as value and `table_key` as key.

    - `table_name` - A string or atom with the name of the table.

    - `specifications` - A keyword list with
      [:mnesia.create_table/2](http://erlang.org/doc/man/mnesia.html#create_table-2)
      table definitions.

  ### Example

  ```elixir
  defmodule MyApp.Database do
    use FluxDatabase

    @impl FluxDatabase
    def config do
      [
        database: :mnesia,
        nodes: [node()],
        tables: %{
          user: {
            :user,
            attributes: [:id, :name, :email]
          },
          post: {
            :post,
            attributes: [:id, :title, :content]
          }
        }
      ]
    end
  end
  ```

  ## Usage

  Whenever you need to use a function defined here, use it without the `tables`
  parameter and by your database module. For example, if you defined your
  database module as `MyApp.Database`, you can use:

  ```elixir
  alias MyApp.Database

  # To start your application
  Database.start()

  # To retrieve an item from table
  Database.read(:user, 1)

  # To match items on table
  Database.match(:user, %{id: :_, name: :_, email: :_})

  # To write an item on table
  Database.write(:user, %{id: 2, name: "John Doe", email: "john@doe.com"})
  ```

  Check the functions defined on this module for more information
  about how to use the functions in your database module.
  """

  @moduledoc since: "0.0.1"

  alias FluxDatabase.Mnesia.{Matcher, Reader, Starter, Writer}

  require Logger

  @doc """
  Create and/or open all tables using
  [:mnesia.create_table/2](http://erlang.org/doc/man/mnesia.html#create_table-2).

  Use it from your database module with no parameters.

  ## Parameters

    - `nodes` - A list with node definitions. More information in
      `FluxDatabase.Mnesia`.

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.Mnesia`.

  ## Examples

      iex> tables = %{user: {:user, attributes: [:id, :name, :email]}}
      ...> FluxDatabase.Mnesia.start([node()], tables)
      :ok
  """

  @doc since: "0.0.1"

  @spec start([atom()], map()) :: :ok | {:error, atom()}
  def start(nodes, tables) do
    Starter.start(nodes, tables)
  end

  @doc """
  Retrieve an item from table using
  [:mnesia.read/1](http://erlang.org/doc/man/mnesia.html#read-1).

  Use it from your database module without `tables` parameter.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.Mnesia`.

    - `table_key` - A string or atom with the name of the table.

    - `id` - The identifier value of the item.

  ## Examples

      iex> tables = %{user: {:user, attributes: [:id, :name, :email]}}
      ...> FluxDatabase.Mnesia.start([node()], tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.Mnesia.write(tables, :user, user)
      ...> FluxDatabase.Mnesia.read(tables, :user, 1)
      {:ok, %{id: 1, name: "John Doe", email: "john@doe.com"}}
  """

  @doc since: "0.0.1"

  @spec read(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def read(tables, table_key, id) do
    Reader.read(tables, table_key, id)
  end

  @doc """
  Match items on table using
  [:mnesia.match_object/1](http://erlang.org/doc/man/mnesia.html#match_object-1).

  Use it from your database module without `tables` parameter.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.Mnesia`.

    - `table_key` - A string or atom with the name of the table.

    - `matcher` - A map with ALL the keys of the table. The value defined in a
      key will be used to match the items. If any value should match, use `:_`.

  ## Examples

      iex> tables = %{user: {:user, attributes: [:id, :name, :email]}}
      ...> FluxDatabase.Mnesia.start([node()], tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.Mnesia.write(tables, :user, user)
      ...> user = %{id: 2, name: "John Anderson", email: "neo@matrix.com"}
      ...> FluxDatabase.Mnesia.write(tables, :user, user)
      ...> FluxDatabase.Mnesia.match(tables, :user, %{id: :_, name: :_, email: :_})
      {
        :ok,
        [
          %{id: 1, name: "John Doe", email: "john@doe.com"},
          %{id: 2, name: "John Anderson", email: "neo@matrix.com"}
        ]
      }
  """

  @doc since: "0.0.1"

  @spec match(map(), atom() | binary(), map()) :: {:ok, [map()]} | {:error, atom()}
  def match(tables, table_key, matcher) do
    Matcher.match(tables, table_key, matcher)
  end

  @doc """
  Write an item on table using
  [:mnesia.write/1](http://erlang.org/doc/man/mnesia.html#write-1).

  It merges the new data with the data written on table before writing the new
  data.

  Use it from your database module without `tables` parameter.

  ## Parameters

    - `tables` - A map with tables settings. More information in
      `FluxDatabase.Mnesia`.

    - `table_key` - A string or atom with the name of the table.

    - `data` - A map with the values that will be inserted on storage. Must
      have ALL keys defined on table settings.

  ## Examples

      iex> tables = %{user: {:user, attributes: [:id, :name, :email]}}
      ...> FluxDatabase.Mnesia.start([node()], tables)
      ...> user = %{id: 1, name: "John Doe", email: "john@doe.com"}
      ...> FluxDatabase.Mnesia.write(tables, :user, user)
      {:ok, %{id: 1, name: "John Doe", email: "john@doe.com"}}
  """

  @doc since: "0.0.1"

  @spec write(map(), atom() | binary(), map()) :: {:ok, map()} | {:error, atom()}
  def write(tables, table_key, data) do
    Writer.write(tables, table_key, data)
  end
end
