defmodule FluxDatabase.Mnesia.Matcher do
  @moduledoc false

  alias FluxDatabase.Mnesia.Utils

  require Logger

  @spec match(map(), atom() | binary(), map()) :: {:ok, [map()]} | {:error, atom()}
  def match(tables, table_key, matcher) do
    with {:ok, {table_name, specifications}} <- Utils.get_table(tables, table_key),
         attributes when is_list(attributes) <- Keyword.get(specifications, :attributes),
         {:ok, tuple_to_match} <- Utils.convert_map_to_tuple(table_name, matcher, attributes),
         {:ok, function_to_match} <- build_matcher_function(tuple_to_match),
         :ok <- :mnesia.wait_for_tables([table_name], 5_000),
         {:atomic, elements} <- :mnesia.transaction(function_to_match),
         {:ok, maps} <- Utils.convert_tuples_to_maps(table_name, elements, attributes) do
      {:ok, maps}
    else
      error ->
        Logger.error("Mnesia failed to perform match operation",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_key: table_key,
              matcher: matcher
            )
        )

        {:error, :mnesia_failed_to_match}
    end
  end

  defp build_matcher_function(tuple_to_match) do
    {:ok, fn -> :mnesia.match_object(tuple_to_match) end}
  end
end
