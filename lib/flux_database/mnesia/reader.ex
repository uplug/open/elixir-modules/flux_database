defmodule FluxDatabase.Mnesia.Reader do
  @moduledoc false

  alias FluxDatabase.Mnesia.Utils

  require Logger

  @spec read(map(), atom() | binary(), any()) :: {:ok, map()} | {:error, atom()}
  def read(tables, table_key, id) do
    with {:ok, {table_name, specifications}} <- Utils.get_table(tables, table_key),
         {:ok, tuple_to_read} <- build_read_tuple(table_name, id),
         {:ok, function_to_read} <- build_read_function(tuple_to_read),
         :ok <- :mnesia.wait_for_tables([table_name], 5_000),
         {:atomic, [element]} <- :mnesia.transaction(function_to_read),
         attributes when is_list(attributes) <- Keyword.get(specifications, :attributes),
         {:ok, map} <- Utils.convert_tuple_to_map(table_name, element, attributes) do
      {:ok, map}
    else
      {:atomic, []} ->
        {:error, :not_found}

      error ->
        Logger.error("Mnesia failed to read",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_key: table_key,
              id: id
            )
        )

        {:error, :mnesia_failed_to_read}
    end
  end

  defp build_read_tuple(table_name, id) do
    {:ok, {table_name, id}}
  end

  defp build_read_function(tuple_to_read) do
    {:ok, fn -> :mnesia.read(tuple_to_read) end}
  end
end
