defmodule FluxDatabase.Mnesia.Starter do
  @moduledoc false

  require Logger

  @spec start([atom()], map()) :: :ok | {:error, atom()}
  def start(nodes, tables) do
    with :ok <- create_schema(nodes),
         :ok <- change_table_copy_type(),
         :ok <- :mnesia.start(),
         :ok <- create_tables(tables) do
      Logger.info("Mnesia database ready to use",
        result: :ok,
        flux_database_params:
          inspect(
            nodes: nodes,
            tables: tables
          )
      )

      :ok
    else
      error ->
        Logger.error("Mnesia database failed to start",
          error: inspect(error),
          flux_database_params:
            inspect(
              nodes: nodes,
              tables: tables
            )
        )

        error
    end
  end

  defp change_table_copy_type do
    case :mnesia.change_table_copy_type(:schema, node(), :disc_copies) do
      {:atomic, :ok} ->
        # Logger.debug("Table copy changed")
        :ok

      {:aborted, {:already_exists, :schema, :nonode@nohost, :disc_copies}} ->
        # Logger.debug("Already disc_copies")
        :ok

      {:aborted, _args} ->
        # Logger.debug("Error")
        :ok
    end
  end

  defp create_schema(nodes) do
    case :mnesia.create_schema(nodes) do
      :ok -> :ok
      {:error, {host, {:already_exists, host}}} -> :ok
      error -> error
    end
  end

  defp create_tables(tables) do
    Enum.each(tables, &create_table!/1)
  rescue
    _error -> {:error, :failed_to_create_tables}
  end

  defp create_table!({_key, {table_name, specifications}}) do
    args = [{:disc_copies, [node()]}] ++ specifications

    case :mnesia.create_table(table_name, args) do
      {:atomic, :ok} ->
        :ok

      {:aborted, {:already_exists, _table_name}} ->
        :ok

      {:aborted, _reason} = error ->
        Logger.error("Mnesia failed to create table",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_name: table_name,
              specifications: specifications
            )
        )

        raise ":mnesia.create_table/2 failed"
    end
  end
end
