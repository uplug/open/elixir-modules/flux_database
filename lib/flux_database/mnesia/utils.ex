defmodule FluxDatabase.Mnesia.Utils do
  @moduledoc false

  require Logger

  @spec convert_map_to_tuple(atom() | binary(), map(), [atom() | binary()]) :: tuple()
  def convert_map_to_tuple(table_name, map, attributes) do
    {
      :ok,
      Enum.reduce(
        attributes,
        {table_name},
        fn attribute, tuple -> Tuple.append(tuple, Map.get(map, attribute)) end
      )
    }
  rescue
    _error -> {:error, :failed_to_convert_map_to_tuple}
  end

  @spec convert_tuples_to_maps(atom() | binary(), [tuple()], [atom() | binary()]) ::
          {:ok, [map()]}
  def convert_tuples_to_maps(table_name, tuples, attributes) do
    {
      :ok,
      Enum.map(
        tuples,
        fn tuple ->
          {:ok, map} = convert_tuple_to_map(table_name, tuple, attributes)
          map
        end
      )
    }
  end

  @spec convert_tuple_to_map(atom() | binary(), tuple(), [atom() | binary()]) :: {:ok, map()}
  def convert_tuple_to_map(table_name, tuple, attributes) do
    fields = Enum.zip(attributes, List.delete(Tuple.to_list(tuple), table_name))

    {
      :ok,
      Enum.reduce(
        fields,
        %{},
        fn {key, value}, map -> Map.put(map, key, value) end
      )
    }
  end

  @spec get_table(map(), atom() | binary()) ::
          {:ok, {atom() | binary(), [atom() | binary()]}} | {:error, atom()}
  def get_table(tables, table_key) do
    case Map.get(tables, table_key) do
      {_table_name, _specifications} = result -> {:ok, result}
      nil -> {:error, :mnesia_table_not_found}
      _ -> {:error, :mnesia_wrong_tables_structure}
    end
  end
end
