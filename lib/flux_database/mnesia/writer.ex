defmodule FluxDatabase.Mnesia.Writer do
  @moduledoc false

  alias FluxDatabase.Mnesia.{Reader, Utils}

  require Logger

  @spec write(map(), atom() | binary(), map()) :: {:ok, map()} | {:error, atom()}
  def write(tables, table_key, %{id: id} = data) do
    case Reader.read(tables, table_key, id) do
      {:ok, saved_data} -> do_write(tables, table_key, Map.merge(saved_data, data))
      {:error, :not_found} -> do_write(tables, table_key, data)
      _error -> {:error, :mnesia_failed_to_write}
    end
  end

  def write(tables, table_key, data) do
    do_write(tables, table_key, data)
  end

  defp do_write(tables, table_key, data) do
    with {:ok, {table_name, specifications}} <- Utils.get_table(tables, table_key),
         attributes when is_list(attributes) <- Keyword.get(specifications, :attributes),
         {:ok, tuple_to_write} <- Utils.convert_map_to_tuple(table_name, data, attributes),
         {:ok, function_to_write} <- build_write_function(tuple_to_write),
         :ok <- :mnesia.wait_for_tables([table_name], 5_000),
         {:atomic, :ok} <- :mnesia.transaction(function_to_write) do
      {:ok, data}
    else
      error ->
        Logger.error("Mnesia failed to write data",
          error: inspect(error),
          flux_database_params:
            inspect(
              table_key: table_key,
              data: data
            )
        )

        {:error, :mnesia_failed_to_write}
    end
  end

  defp build_write_function(tuple_to_write) do
    {:ok, fn -> :mnesia.write(tuple_to_write) end}
  end
end
