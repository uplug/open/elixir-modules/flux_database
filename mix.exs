defmodule FluxDatabase.MixProject do
  use Mix.Project

  @version "0.0.3"
  @source_url "https://gitlab.com/uplug/open/elixir-modules/flux_database"

  def project do
    [
      app: :flux_database,
      name: "Flux Database",
      description: "Integrate databases to Elixir projects.",
      source_url: @source_url,
      version: @version,
      elixir: "~> 1.9",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      deps: deps(),
      docs: docs(),
      package: package(),
      preferred_cli_env: preferred_cli_env()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      "test.all": ["test.static", "test.coverage"],
      "test.coverage": ["coveralls"],
      "test.static": ["format --check-formatted", "credo list --strict --all"]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.1.5", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21.2", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12.0", only: [:dev, :test]},
      {:jason, "~> 1.1.2"}
    ]
  end

  defp docs do
    [
      main: "readme",
      authors: ["Jonathan Moraes"],
      extras: ~w(CHANGELOG.md README.md)
    ]
  end

  defp package do
    [
      maintainers: ["Eduardo Pinheiro", "Jonathan Moraes"],
      licenses: ["Apache-2.0"],
      files: ~w(lib mix.exs CHANGELOG.md LICENSE README.md),
      links: %{"GitLab" => @source_url}
    ]
  end

  defp preferred_cli_env do
    [
      coveralls: :test,
      "coveralls.detail": :test,
      "coveralls.html": :test,
      "coveralls.post": :test,
      "test.all": :test,
      "test.coverage": :test,
      "test.static": :test
    ]
  end
end
