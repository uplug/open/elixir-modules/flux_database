defmodule FluxDatabase.DETSTest do
  use ExUnit.Case

  alias FluxDatabase.DETS

  @moduletag :capture_log

  def matchspecs({_, data}), do: data

  describe "start/2" do
    test "returns :ok if everything went fine" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      assert DETS.start(tables) == :ok
    end

    test "returns an error if something went wrong" do
      tables = %{
        test: {"not atom", [type: :set], :id}
      }

      refute DETS.start(tables) == :ok

      tables = %{
        test: {:"/test", [type: :unknown], :id}
      }

      refute DETS.start(tables) == :ok
    end
  end

  describe "write/3" do
    test "returns {:ok, data} if everything went fine" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)

      assert DETS.write(tables, table_key, data) == {:ok, data}
    end

    test "returns {:error, :dets_failed_to_write} if something went wrong" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)

      assert DETS.write(tables, table_key, data) == {:error, :dets_failed_to_write}
    end
  end

  describe "read/3" do
    test "returns {:ok, map} if everything went fine" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)
      DETS.write(tables, table_key, data)

      assert DETS.read(tables, table_key, 1) == {:ok, data}
    end

    test "returns {:error, :not_found} there are no element with the identifier" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)
      DETS.write(tables, table_key, data)

      assert DETS.read(tables, table_key, 2) == {:error, :not_found}
    end

    test "returns error if something went wrong" do
      tables = %{
        test: {:"/test", [type: :set]}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)
      DETS.write(tables, table_key, data)

      table_key = :unknown

      assert DETS.read(tables, table_key, 1) == {:error, :dets_failed_to_get_table_data}
    end
  end

  describe "filter/3" do
    test "returns {:ok, []} if everything went fine" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)
      DETS.write(tables, table_key, data)

      assert DETS.filter(tables, table_key, DETS.all_matchspecs()) == {:ok, [data]}
    end

    test "returns error if something went wrong" do
      tables = %{
        test: {:"/test", [type: :set], :id}
      }

      table_key = :test

      data = %{
        id: 1,
        cat: :meow,
        dog: "woof"
      }

      DETS.start(tables)
      DETS.write(tables, table_key, data)

      table_key = :unknown

      assert DETS.filter(tables, table_key, DETS.all_matchspecs()) ==
               {:error, :dets_failed_to_get_table_data}

      table_key = :test

      assert DETS.filter(tables, table_key, []) == {:error, :dets_failed_to_filter}
    end
  end
end
