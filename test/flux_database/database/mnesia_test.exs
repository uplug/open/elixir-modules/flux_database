defmodule FluxDatabase.MnesiaTest do
  use ExUnit.Case

  alias FluxDatabase.Mnesia

  @moduletag :capture_log

  describe "start/2" do
    test "returns :ok if everything went fine" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      assert Mnesia.start(nodes, tables) == :ok
      assert Mnesia.start(nodes, tables) == :ok
    end

    test "returns an error if something went wrong" do
      nodes = [node()]

      tables = %{
        test: {
          :error,
          attributes: nil
        }
      }

      refute Mnesia.start(nodes, tables) == :ok

      nodes = nil

      refute Mnesia.start(nodes, tables) == :ok
    end
  end

  describe "write/3" do
    test "returns {:ok, data} if everything went fine" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :test

      data = %{
        id: 1,
        animal: "cat",
        sound: "meow"
      }

      assert Mnesia.write(tables, table_key, data) == {:ok, data}
    end

    test "returns {:error, :mnesia_failed_to_write} if something went wrong" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :test
      data = nil

      assert Mnesia.write(tables, table_key, data) == {:error, :mnesia_failed_to_write}

      table_key = nil
      data = %{id: 1}
      assert Mnesia.write(tables, table_key, data) == {:error, :mnesia_failed_to_write}
    end
  end

  describe "read/3" do
    test "returns {:ok, map} if everything went fine" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :test

      data = %{
        id: 1,
        animal: "cat",
        sound: "meow"
      }

      Mnesia.write(tables, table_key, data)

      id = 1

      assert Mnesia.read(tables, table_key, id) == {:ok, %{id: 1, animal: "cat", sound: "meow"}}
    end

    test "returns {:error, :not_found} there are no element with the identifier" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :test
      id = "dog"

      assert Mnesia.read(tables, table_key, id) == {:error, :not_found}
    end

    test "returns {:error, :mnesia_failed_to_read} if something went wrong" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :error
      id = "dog"

      assert Mnesia.read(tables, table_key, id) == {:error, :mnesia_failed_to_read}

      tables = %{
        test: {
          :test,
          [attributes: [:id, :animal, :sound]],
          :unexpected_element
        }
      }

      table_key = :test

      assert Mnesia.read(tables, table_key, id) == {:error, :mnesia_failed_to_read}
    end
  end

  describe "match/3" do
    test "returns {:ok, maps} if everything went fine" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :test

      data = %{
        id: 1,
        animal: "cat",
        sound: "meow"
      }

      data2 = %{
        id: 2,
        animal: "lynx",
        sound: "meow"
      }

      Mnesia.write(tables, table_key, data)
      Mnesia.write(tables, table_key, data2)

      matcher = %{id: :_, animal: :_, sound: "meow"}

      assert Mnesia.match(tables, table_key, matcher) == {
               :ok,
               [
                 %{id: 1, animal: "cat", sound: "meow"},
                 %{id: 2, animal: "lynx", sound: "meow"}
               ]
             }
    end

    test "returns {:error, :mnesia_failed_to_match} if something went wrong" do
      nodes = [node()]

      tables = %{
        test: {
          :test,
          attributes: [:id, :animal, :sound]
        }
      }

      Mnesia.start(nodes, tables)

      table_key = :error
      matcher = %{id: :_, animal: :_, sound: "meow"}

      assert Mnesia.match(tables, table_key, matcher) == {:error, :mnesia_failed_to_match}
    end
  end
end
